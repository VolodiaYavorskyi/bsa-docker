FROM nginx:alpine

COPY index.html /usr/share/nginx/html
COPY css /usr/share/nginx/html/css
COPY fontawesome /usr/share/nginx/html/fontawesome
COPY images /usr/share/nginx/html/images

EXPOSE 80